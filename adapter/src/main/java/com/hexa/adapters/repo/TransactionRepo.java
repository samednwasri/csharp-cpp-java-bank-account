package com.hexa.adapters.repo;

import com.hexa.adapters.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface TransactionRepo extends JpaRepository<Transaction, Long> {

  List<Transaction> findAllByAccountId(Long accountId);
}
