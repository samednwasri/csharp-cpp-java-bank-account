package com.hexa.adapters.jpaadapters;

import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.ports.jpa.TransactionJpaPort;
import com.hexa.adapters.repo.TransactionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.hexa.adapters.mapper.TransactionJpaMapper.TRANSACTION_JPA_MAPPER;
@Service
@RequiredArgsConstructor
public class TransactionJpaAdapter implements TransactionJpaPort {
    private final TransactionRepo _transactionRepo;


    @Override
    public TransactionModel findById(Long id) {
    return TRANSACTION_JPA_MAPPER.toDomainModel(_transactionRepo.getReferenceById(id));
    }

    @Override
    public List<TransactionModel> findAllByAccountId(Long accountId) {
        return TRANSACTION_JPA_MAPPER.toDomainModelList(_transactionRepo.findAllByAccountId(accountId));
    }

    @Override
    public TransactionModel save(TransactionModel transactionModel) {
        return TRANSACTION_JPA_MAPPER.toDomainModel(_transactionRepo.save(TRANSACTION_JPA_MAPPER.fromDomainModel(transactionModel)));
    }
}
