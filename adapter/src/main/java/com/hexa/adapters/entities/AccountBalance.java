package com.hexa.adapters.entities;

import jakarta.persistence.*;

import java.math.BigDecimal;

import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class AccountBalance {

  @Id
  @Column(nullable = false, updatable = false)
  @SequenceGenerator(
      name = "primary_sequence",
      sequenceName = "primary_sequence",
      allocationSize = 1,
      initialValue = 10000)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "primary_sequence")
  private Long id;

  @Column(precision = 10, scale = 2)
  private BigDecimal credit;

  @Column(precision = 10, scale = 2)
  private BigDecimal debit;

  @OneToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "account_id", referencedColumnName = "id")
  private Account account;
}
