package com.hexa.domain.core.balances;

import static com.hexa.domain.core.AccountModelTestBuilder.getDefaultAccount;
import static java.util.stream.IntStream.range;

import com.hexa.domain.model.AccountBalanceModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountBalanceModelTestBuilder {

  public static final long TEST_ID = 1;
  public static final BigDecimal DEFAULT_CREDIT_AMOUNT = BigDecimal.TEN;
  public static final BigDecimal DEFAULT_DEBIT_AMOUNT = BigDecimal.ONE;

  public static AccountBalanceModel getDefaultAccountBalance() {
    return getDefaultAccountBalanceBuilder().build();
  }

  public static AccountBalanceModel.AccountBalanceModelBuilder getDefaultAccountBalanceBuilder() {
    return AccountBalanceModel.builder()
        .id(TEST_ID)
        .debit(DEFAULT_DEBIT_AMOUNT)
        .credit(DEFAULT_CREDIT_AMOUNT)
        .account(getDefaultAccount());
  }

  public static List<AccountBalanceModel> buildMany(Integer amount) {
    List<AccountBalanceModel> result = new ArrayList<>();
    range(1, amount + 1)
        .forEach(
            i -> {
              AccountBalanceModel acc =
                  AccountBalanceModel.builder()
                      .id((long) i)
                      .debit(DEFAULT_DEBIT_AMOUNT)
                      .credit(DEFAULT_CREDIT_AMOUNT)
                      .account(getDefaultAccount())
                      .build();
              result.add(acc);
            });
    return result;
  }
}
