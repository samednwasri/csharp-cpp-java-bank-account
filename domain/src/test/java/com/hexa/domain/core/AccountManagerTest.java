package com.hexa.domain.core;

import com.hexa.domain.core.balances.AccountBalanceModelTestBuilder;
import com.hexa.domain.core.balances.BalanceManger;
import com.hexa.domain.core.transactions.TransactionManger;
import com.hexa.domain.core.transactions.TransactionModelTestBuilder;
import com.hexa.domain.exception.AccountNotFoundException;
import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.model.AccountModel;
import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.ports.jpa.AccountBalanceJpaPort;
import com.hexa.domain.ports.jpa.AccountJpaPort;
import com.hexa.domain.ports.jpa.TransactionJpaPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountManagerTest {
  @Mock public AccountJpaPort accountJpaPort;
  @Mock public AccountBalanceJpaPort accountBalanceJpaPort;
  @Mock public TransactionJpaPort transactionJpaPort;
  @InjectMocks private BalanceManger balanceManger;
  @InjectMocks private TransactionManger transactionManger;

  @Test
  void testDepositMoney() throws AccountNotFoundException {
      AccountManager accountManager =
              new AccountManager(transactionManger, accountJpaPort, balanceManger);
      AccountModel account = AccountModelTestBuilder.getDefaultAccount();
      TransactionModel returnedTransaction =
              TransactionModelTestBuilder.getDefaultTransaction(BigDecimal.ONE.negate());

      when(accountJpaPort.findById(account.getId())).thenReturn(account);
      when(transactionManger.createTransaction(account,returnedTransaction.getAmount(), any()))
              .thenReturn(returnedTransaction);
      assertThat(accountManager.depositMoney(account.getId(), BigDecimal.ONE.negate()))
              .usingRecursiveAssertion()
              .isEqualTo(returnedTransaction);
  }

  @Test
  void testRetrieveMoney() throws AccountNotFoundException {
    AccountManager accountManager =
        new AccountManager(transactionManger, accountJpaPort, balanceManger);
    AccountModel account = AccountModelTestBuilder.getDefaultAccount();
    TransactionModel transaction =
        TransactionModelTestBuilder.getDefaultTransaction(BigDecimal.ONE);
    when(accountJpaPort.findById(account.getId())).thenReturn(account);
    when(transactionManger.createTransaction(account, transaction.getAmount(), any()))
        .thenReturn(transaction);
    assertThat(accountManager.retrieveMoney(account.getId(), transaction.getAmount()))
        .usingRecursiveAssertion()
        .isEqualTo(transaction);
  }
}
