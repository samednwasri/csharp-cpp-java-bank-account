package com.hexa.domain.core;

import static java.util.stream.IntStream.range;

import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.model.AccountModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountModelTestBuilder {

  public static final long TEST_ID = 1;
  public static final String TEST_ACCOUNT_NAME = "test account";
  public static final String TEST_ACCOUNT_OWNER_NAME = "test account owner";

  public static AccountModel getDefaultAccount() {
    return AccountModel.builder()
        .id(TEST_ID)
        .name(TEST_ACCOUNT_NAME)
        .owner(TEST_ACCOUNT_OWNER_NAME)
        .build();
  }

  public static List<AccountModel> buildMany(Integer amount) {
    List<AccountModel> result = new ArrayList<>();
    range(1, amount + 1)
        .forEach(
            i -> {
              AccountModel acc =
                  AccountModel.builder()
                      .id((long) i)
                      .name(TEST_ACCOUNT_NAME + " " + i)
                      .owner(TEST_ACCOUNT_OWNER_NAME)
                      .build();
              result.add(acc);
            });
    return result;
  }
}
