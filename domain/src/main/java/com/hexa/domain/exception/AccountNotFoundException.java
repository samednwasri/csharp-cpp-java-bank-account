package com.hexa.domain.exception;

public class AccountNotFoundException extends NotFoundException{
    public AccountNotFoundException(String msg){
        super(msg);
    }
}
