package com.hexa.domain.ports.jpa;


import com.hexa.domain.model.TransactionModel;

import java.util.List;

public interface TransactionJpaPort {

    TransactionModel findById(Long id);

    List<TransactionModel> findAllByAccountId(Long accountId);

    TransactionModel save(TransactionModel transactionModel);
}
