package com.hexa.domain.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Data
public class TransactionModel {

  private Long id;
  private OffsetDateTime transactionDate;
  private BigDecimal amount;
  private AccountModel account;

}
