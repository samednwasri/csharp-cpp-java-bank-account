package com.hexa.domain.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Data
public class AccountBalanceModel {

  private Long id;
  private BigDecimal credit;
  private BigDecimal debit;
  private AccountModel account;

}
