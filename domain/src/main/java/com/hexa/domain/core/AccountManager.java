package com.hexa.domain.core;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import com.hexa.domain.core.balances.BalanceManger;
import com.hexa.domain.core.transactions.TransactionManger;
import com.hexa.domain.exception.AccountNotFoundException;
import com.hexa.domain.model.AccountModel;
import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.ports.jpa.AccountJpaPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountManager {

  private final TransactionManger transactionManger;
  private final AccountJpaPort accountJpaPort;
  private final BalanceManger balanceManger;

  public TransactionModel depositMoney(Long accountId, BigDecimal amount) throws AccountNotFoundException {
    AccountModel accountModel = getAccount(accountId);
    TransactionModel transactionModel =
        transactionManger.createTransaction(accountModel, amount, OffsetDateTime.now());
    balanceManger.createOrUpdateCurrentBalance(transactionModel);
    return transactionModel;
  }

  public TransactionModel retrieveMoney(Long accountId, BigDecimal amount) throws AccountNotFoundException {
    return depositMoney(accountId, amount.negate());
  }

  public AccountBalanceModel getCurrentBalance(Long accountId) {
    return balanceManger
        .getCurrentBalanceForAccount(accountId);
  }

   public List<TransactionModel> getAllTransactions(Long accountId){
     return transactionManger.getAllTransactions(accountId);
  }
  public AccountModel createAccount(AccountModel accountModel){
    return accountJpaPort.save(accountModel);
  }

  private AccountModel getAccount(Long accountId) throws AccountNotFoundException {
    AccountModel acc= accountJpaPort
        .findById(accountId);
    if(acc==null)
      throw new AccountNotFoundException("Cannot find Account with id : "+accountId);
    return acc;
  }
}
