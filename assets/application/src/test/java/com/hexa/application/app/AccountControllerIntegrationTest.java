package com.hexa.application.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hexa.application.app.controller.AccountController;
import com.hexa.domain.core.AccountManager;
import com.hexa.domain.core.AccountModelTestBuilder;
import com.hexa.domain.core.balances.AccountBalanceModelTestBuilder;
import com.hexa.domain.core.balances.BalanceManger;
import com.hexa.domain.core.transactions.TransactionManger;
import com.hexa.domain.core.transactions.TransactionModelTestBuilder;
import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.model.AccountModel;
import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.ports.jpa.AccountJpaPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import static com.hexa.domain.core.AccountModelTestBuilder.getDefaultAccount;
import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(value = SpringExtension.class)
@WebMvcTest(controllers = AccountController.class)
@ContextConfiguration(classes = {AccountController.class})

public class AccountControllerIntegrationTest {
  public static final int DEFAULT_TRANSACTIONS_PER_ACCOUNT_COUNT = 10;
  private final String BASE_ENDPOINT = "/api/accounts";

  private final String CURRENT_BALANCE_API = "/get-current-balance";
  private final String CREATE_ACCOUNT_API = "/create-account";
  private final String RETRIEVE_MONEY_API = "/retrieve-money";
  private final String DEPOSIT_MONEY_API = "/deposit-money";
  private final String GET_ALL_TRANSACTIONS_API = "/get-transactions";

  @Autowired private MockMvc mvc;


  @Autowired private ObjectMapper objectMapper;

  @MockBean
  private AccountManager _accountManager;

  @Test
  public void testCreateAccount() throws Exception {
    AccountModel accountModel = getDefaultAccount();
    when(_accountManager.createAccount(accountModel)).thenReturn(accountModel);
    mvc.perform(post(BASE_ENDPOINT+CREATE_ACCOUNT_API)
            .contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(accountModel)))
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(accountModel)));
  }

  @Test
  public void testGetAllTransactionsForAccount() throws Exception {
    AccountModel accountModel = getDefaultAccount();
    List<TransactionModel> transactions =
        TransactionModelTestBuilder.buildMany(DEFAULT_TRANSACTIONS_PER_ACCOUNT_COUNT);
    when(_accountManager.getAllTransactions(accountModel.getId())).thenReturn(transactions);
    mvc.perform(get(BASE_ENDPOINT+"/"+accountModel.getId()+GET_ALL_TRANSACTIONS_API))
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(transactions)));
  }

  @Test
  public void testDepositMoney() throws Exception {
    AccountModel accountModel = getDefaultAccount();
    BigDecimal transactionAmount = BigDecimal.TEN;
    TransactionModel transaction =
            TransactionModelTestBuilder.getDefaultTransaction(transactionAmount);
    when(_accountManager.depositMoney(accountModel.getId(),BigDecimal.TEN)).thenReturn(transaction);
    mvc.perform(post(BASE_ENDPOINT+"/"+accountModel.getId()+DEPOSIT_MONEY_API).param("amount", String.valueOf(BigDecimal.TEN)))
            .andExpect(status().isOk())
            .andExpect(content().json(objectMapper.writeValueAsString(transaction)));
  }

  @Test
  public void testRetrieveMoney() throws Exception {
    AccountModel accountModel = getDefaultAccount();
    BigDecimal transactionAmount = BigDecimal.TEN;
    TransactionModel transaction =
            TransactionModelTestBuilder.getDefaultTransaction(transactionAmount.negate());
    when(_accountManager.retrieveMoney(accountModel.getId(),BigDecimal.TEN)).thenReturn(transaction);
    mvc.perform(post(BASE_ENDPOINT+"/"+accountModel.getId()+RETRIEVE_MONEY_API).param("amount", String.valueOf(transactionAmount)))
            .andExpect(status().isOk())
            .andExpect(content().json(objectMapper.writeValueAsString(transaction)));
  }

  @Test
  public void testGetCurrentBalance() throws Exception {
    AccountModel accountModel = getDefaultAccount();
    AccountBalanceModel balanceModel = AccountBalanceModelTestBuilder.getDefaultAccountBalance();
    when(_accountManager.getCurrentBalance(accountModel.getId())).thenReturn(balanceModel);
    mvc.perform(get(BASE_ENDPOINT+"/"+accountModel.getId()+CURRENT_BALANCE_API))
            .andExpect(status().isOk())
            .andExpect(content().json(objectMapper.writeValueAsString(balanceModel)));
  }

}
