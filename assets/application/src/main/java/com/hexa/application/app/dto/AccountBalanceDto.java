package com.hexa.application.app.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccountBalanceDto {

  private Long id;
  private BigDecimal credit;
  private BigDecimal debit;
  private AccountDto account;

}
