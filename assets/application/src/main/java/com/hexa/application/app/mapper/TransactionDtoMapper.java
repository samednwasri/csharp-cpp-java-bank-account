package com.hexa.application.app.mapper;


import static org.mapstruct.factory.Mappers.getMapper;

import com.hexa.application.app.dto.TransactionDto;
import com.hexa.domain.model.TransactionModel;

import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface TransactionDtoMapper {
    TransactionDtoMapper TRANSACTION_DTO_MAPPER=getMapper(TransactionDtoMapper.class);

    TransactionModel toDomainModel(TransactionDto accountBalance);

    TransactionDto fromDomainModel(TransactionModel transactionModel);
    List<TransactionModel> toDomainModelList(List<TransactionDto> transactionList);

    List<TransactionDto> fromDomainModelList(List<TransactionModel> transactionList);


}
