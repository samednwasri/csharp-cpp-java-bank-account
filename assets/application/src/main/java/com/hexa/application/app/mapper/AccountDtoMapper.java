package com.hexa.application.app.mapper;


import static org.mapstruct.factory.Mappers.getMapper;

import com.hexa.domain.model.AccountModel;
import com.hexa.application.app.dto.AccountDto;
import org.mapstruct.Mapper;

@Mapper
public interface AccountDtoMapper {
    AccountDtoMapper ACCOUNT_DTO_MAPPER=getMapper(AccountDtoMapper.class);

    AccountModel toDomainModel(AccountDto account);

    AccountDto fromDomainModel(AccountModel accountModel);


}
