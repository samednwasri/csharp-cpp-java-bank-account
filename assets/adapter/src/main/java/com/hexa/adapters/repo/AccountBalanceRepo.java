package com.hexa.adapters.repo;

import com.hexa.adapters.entities.Account;
import com.hexa.adapters.entities.AccountBalance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Repository
public interface AccountBalanceRepo extends JpaRepository<AccountBalance,Long> {
    Optional<AccountBalance> findFirstByAccountId(Long id);

    void deleteByAccount(Account account);

}
