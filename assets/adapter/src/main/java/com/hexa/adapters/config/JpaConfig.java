package com.hexa.adapters.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.boot.autoconfigure.domain.EntityScan;
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages={"com.hexa.adapters.repo"})
@EntityScan("com.hexa.adapters.entities")
public class JpaConfig {

}
