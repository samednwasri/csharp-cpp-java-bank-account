package com.hexa.adapters.mapper;


import com.hexa.adapters.entities.AccountBalance;
import com.hexa.domain.model.AccountBalanceModel;
import org.mapstruct.Mapper;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper
public interface AccountBalanceJpaMapper {
    AccountBalanceJpaMapper ACCOUNTBALANCE_JPA_MAPPER=getMapper(AccountBalanceJpaMapper.class);

    AccountBalanceModel toDomainModel(AccountBalance accountBalance);

    AccountBalance fromDomainModel(AccountBalanceModel accountModel);

}
