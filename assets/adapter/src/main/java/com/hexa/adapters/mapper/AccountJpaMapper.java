package com.hexa.adapters.mapper;


import static org.mapstruct.factory.Mappers.getMapper;

import com.hexa.domain.model.AccountModel;
import com.hexa.adapters.entities.Account;
import org.mapstruct.Mapper;

@Mapper
public interface AccountJpaMapper {
    AccountJpaMapper ACCOUNT_JPA_MAPPER=getMapper(AccountJpaMapper.class);

    AccountModel toDomainModel(Account accountBalance);

    Account fromDomainModel(AccountModel accountModel);


}
