package com.hexa.adapters.jpaadapters;

import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.ports.jpa.AccountBalanceJpaPort;
import com.hexa.adapters.repo.AccountBalanceRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.hexa.adapters.mapper.AccountBalanceJpaMapper.ACCOUNTBALANCE_JPA_MAPPER;
@Service
@RequiredArgsConstructor
public class AccountBalanceJpaAdapter implements AccountBalanceJpaPort {
    private final AccountBalanceRepo _accountBalanceRepo ;
    @Override
    public AccountBalanceModel findFirstByAccountId(Long id) {
        return ACCOUNTBALANCE_JPA_MAPPER.toDomainModel(_accountBalanceRepo.findFirstByAccountId(id).orElse(null));
    }

    @Override
    public AccountBalanceModel save(AccountBalanceModel accountBalanceModel) {
        return ACCOUNTBALANCE_JPA_MAPPER.toDomainModel(_accountBalanceRepo.save(ACCOUNTBALANCE_JPA_MAPPER.fromDomainModel(accountBalanceModel)));
    }
}
