package com.hexa.adapters.repo;

import com.hexa.adapters.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public interface AccountRepo extends JpaRepository<Account,Long> {}
