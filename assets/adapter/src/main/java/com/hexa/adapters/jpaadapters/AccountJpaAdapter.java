package com.hexa.adapters.jpaadapters;

import com.hexa.domain.model.AccountModel;
import com.hexa.domain.ports.jpa.AccountJpaPort;
import com.hexa.adapters.repo.AccountRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.hexa.adapters.mapper.AccountJpaMapper.ACCOUNT_JPA_MAPPER;
@Service
@RequiredArgsConstructor
public class AccountJpaAdapter implements AccountJpaPort {
    private final AccountRepo _accountRepo;
    @Override
    public AccountModel findById(Long id) {
        return ACCOUNT_JPA_MAPPER.toDomainModel(_accountRepo.findById(id).orElse(null));
    }

    @Override
    public AccountModel save(AccountModel accountModel){
        return ACCOUNT_JPA_MAPPER.toDomainModel(_accountRepo.save(ACCOUNT_JPA_MAPPER.fromDomainModel(accountModel)));
    }

}
