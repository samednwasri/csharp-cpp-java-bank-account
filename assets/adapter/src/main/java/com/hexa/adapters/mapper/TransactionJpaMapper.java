package com.hexa.adapters.mapper;


import static org.mapstruct.factory.Mappers.getMapper;

import com.hexa.domain.model.TransactionModel;
import com.hexa.adapters.entities.Transaction;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface TransactionJpaMapper {
    TransactionJpaMapper TRANSACTION_JPA_MAPPER=getMapper(TransactionJpaMapper.class);

    TransactionModel toDomainModel(Transaction accountBalance);

    Transaction fromDomainModel(TransactionModel transactionModel);
    List<TransactionModel> toDomainModelList(List<Transaction> transactionList);
    
}
