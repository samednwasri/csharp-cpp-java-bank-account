package com.hexa.domain.core.transactions;

import static com.hexa.domain.core.AccountModelTestBuilder.getDefaultAccount;
import static java.util.stream.IntStream.range;

import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.model.AccountModel;
import com.hexa.domain.model.TransactionModel;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class TransactionModelTestBuilder {

  public static final long TEST_ID = 1;
  public static final BigDecimal DEFAULT_AMOUNT = BigDecimal.TEN;

  public static TransactionModel getDefaultTransaction(BigDecimal amount) {
    return TransactionModel.builder()
        .id(TEST_ID)
        .amount(amount)
        .transactionDate(OffsetDateTime.now())
        .account(getDefaultAccount())
        .build();
  }

  public static TransactionModel getDefaultTransaction() {
    return getDefaultTransaction(DEFAULT_AMOUNT);
  }

  public static List<TransactionModel> buildMany(Integer amount) {
    List<TransactionModel> result = new ArrayList<>();
    range(1, amount + 1)
        .forEach(
            i -> {
              TransactionModel acc =
                  TransactionModel.builder()
                      .id((long) i)
                      .amount(DEFAULT_AMOUNT)
                      .account(getDefaultAccount())
                      .build();
              result.add(acc);
            });
    return result;
  }
}
