package com.hexa.domain.core.balances;

import static com.hexa.domain.core.AccountModelTestBuilder.getDefaultAccount;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

import com.hexa.domain.core.AccountModelTestBuilder;
import com.hexa.domain.core.transactions.TransactionModelTestBuilder;
import com.hexa.domain.model.AccountBalanceModel;
import com.hexa.domain.model.AccountModel;
import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.ports.jpa.AccountBalanceJpaPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(value = SpringExtension.class)
public class BalanceManagerTest {
  @MockBean private AccountBalanceJpaPort accountBalanceJpaPort;

  @Test
  void testCreateBalance() {
    BalanceManger balanceManger = new BalanceManger(accountBalanceJpaPort);
    AccountModel account = AccountModelTestBuilder.getDefaultAccount();
    TransactionModel transaction =
        TransactionModelTestBuilder.getDefaultTransaction(BigDecimal.ONE);
    AccountBalanceModel balance =
        AccountBalanceModel.builder()
            .account(transaction.getAccount())
            .credit(transaction.getAmount())
            .debit(BigDecimal.ZERO)
            .build();
    when(accountBalanceJpaPort.findFirstByAccountId(account.getId())).thenReturn(null);
    when(accountBalanceJpaPort.save(balance)).thenReturn(balance);
    assertThat(balanceManger.createOrUpdateCurrentBalance(transaction))
        .usingRecursiveAssertion()
        .isEqualTo(balance);
  }

  @Test
  void testUpdateBalance() {
    BalanceManger balanceManger = new BalanceManger(accountBalanceJpaPort);
    AccountModel account = AccountModelTestBuilder.getDefaultAccount();
    AccountBalanceModel defaultBalance = AccountBalanceModelTestBuilder.getDefaultAccountBalance();
    AccountBalanceModel updatedBalance =
        AccountBalanceModelTestBuilder.getDefaultAccountBalanceBuilder()
            .credit(defaultBalance.getCredit().add(BigDecimal.ONE))
            .build();
    TransactionModel transaction =
        TransactionModelTestBuilder.getDefaultTransaction(BigDecimal.ONE);
    when(accountBalanceJpaPort.findFirstByAccountId(account.getId())).thenReturn(defaultBalance);
    when(accountBalanceJpaPort.save(updatedBalance)).thenReturn(updatedBalance);
    assertThat(balanceManger.createOrUpdateCurrentBalance(transaction))
        .usingRecursiveAssertion()
        .isEqualTo(updatedBalance);
  }
}
