package com.hexa.domain.exception;

public class NotFoundException extends Exception{
    public NotFoundException(String msg){
        super(msg);
    };
}
