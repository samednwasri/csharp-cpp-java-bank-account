package com.hexa.domain.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Data
@Builder
public class AccountModel {

  private Long id;
  private String owner;
  private String name;

}
