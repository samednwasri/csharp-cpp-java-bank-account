package com.hexa.domain.ports.jpa;


import com.hexa.domain.model.AccountModel;

public interface AccountJpaPort  {
    AccountModel findById(Long id);

    AccountModel save(AccountModel accountModel);

}
