package com.hexa.domain.core.transactions;

import com.hexa.domain.model.AccountModel;
import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.ports.jpa.TransactionJpaPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionManger {

  final TransactionJpaPort transactionJpaPort;

  public TransactionModel createTransaction(
          AccountModel accountModel, BigDecimal amount, OffsetDateTime transactionDateTime) {
    TransactionModel transactionModel =
        TransactionModel.builder()
            .account(accountModel)
            .amount(amount)
            .transactionDate(transactionDateTime)
            .build();
    return transactionJpaPort.save(transactionModel);
  }

  public List<TransactionModel> getAllTransactions(Long accountId) {
    return transactionJpaPort.findAllByAccountId(accountId);
  }
}
