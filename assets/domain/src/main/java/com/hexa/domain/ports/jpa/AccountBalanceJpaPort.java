package com.hexa.domain.ports.jpa;


import com.hexa.domain.model.AccountBalanceModel;

public interface AccountBalanceJpaPort {

  AccountBalanceModel findFirstByAccountId(Long id);

  AccountBalanceModel save(AccountBalanceModel accountBalanceModel);
}
