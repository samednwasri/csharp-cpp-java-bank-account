package com.hexa.domain.core.balances;

import java.math.BigDecimal;

import com.hexa.domain.model.TransactionModel;
import com.hexa.domain.ports.jpa.AccountBalanceJpaPort;
import com.hexa.domain.model.AccountBalanceModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BalanceManger {

  private final AccountBalanceJpaPort accountBalanceJpaPort;

  public AccountBalanceModel createOrUpdateCurrentBalance(TransactionModel transactionModel) {
    AccountBalanceModel currentBalanceForAccount =
        getCurrentBalanceForAccount(transactionModel.getAccount().getId());
    BigDecimal amount = transactionModel.getAmount();
    if (currentBalanceForAccount == null) {
      currentBalanceForAccount =
          AccountBalanceModel.builder()
              .account(transactionModel.getAccount())
              .credit(BigDecimal.ZERO)
              .debit(BigDecimal.ZERO)
              .build();
    }
    if (transactionModel.getAmount().signum() >= 0) {
      currentBalanceForAccount.setCredit(currentBalanceForAccount.getCredit().add(amount));
    } else {
      currentBalanceForAccount.setDebit(currentBalanceForAccount.getDebit().add(amount.abs()));
    }
    return accountBalanceJpaPort.save(currentBalanceForAccount);
  }

  public AccountBalanceModel getCurrentBalanceForAccount(Long accountId) {
    return accountBalanceJpaPort.findFirstByAccountId(accountId);
  }
}
