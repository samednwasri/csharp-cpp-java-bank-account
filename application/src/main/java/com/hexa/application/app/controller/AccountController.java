package com.hexa.application.app.controller;

import com.hexa.domain.exception.AccountNotFoundException;
import com.hexa.application.app.config.MissingEntityException;
import com.hexa.domain.core.AccountManager;
import com.hexa.application.app.dto.AccountBalanceDto;
import com.hexa.application.app.dto.AccountDto;
import com.hexa.application.app.dto.TransactionDto;
import com.hexa.application.app.mapper.AccountBalanceDtoMapper;
import com.hexa.application.app.mapper.AccountDtoMapper;
import com.hexa.application.app.mapper.TransactionDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/accounts")

public class AccountController {
  private final AccountManager _accountManager;

  @GetMapping("/{accountId}/get-current-balance")
  public AccountBalanceDto getBalanceForAccount(@PathVariable long accountId) {
    return AccountBalanceDtoMapper.ACCOUNTBALANCE_DTO_MAPPER.fromDomainModel(_accountManager.getCurrentBalance(accountId));
  }

  @PostMapping("/create-account")
  public AccountDto createAccount(@RequestBody AccountDto account) {
    return AccountDtoMapper.ACCOUNT_DTO_MAPPER.fromDomainModel(_accountManager.createAccount(AccountDtoMapper.ACCOUNT_DTO_MAPPER.toDomainModel(account)));
  }

  @PostMapping("/{accountId}/deposit-money")
  public TransactionDto depositMoney(
      @PathVariable long accountId, @RequestParam BigDecimal amount) {
    try {
      return TransactionDtoMapper.TRANSACTION_DTO_MAPPER.fromDomainModel(_accountManager.depositMoney(accountId, amount));
    } catch (AccountNotFoundException e) {
      throw new MissingEntityException(e.getMessage());
    }
  }

  @PostMapping("/{accountId}/retrieve-money")
  public TransactionDto retrieveMoney(
      @PathVariable long accountId, @RequestParam BigDecimal amount) {
    try {
      return TransactionDtoMapper.TRANSACTION_DTO_MAPPER.fromDomainModel(_accountManager.retrieveMoney(accountId, amount));
    } catch (AccountNotFoundException e) {
      throw new MissingEntityException(e.getMessage());
    }
  }

  @GetMapping("/{accountId}/get-transactions")
  public List<TransactionDto> getAllTransaction(@PathVariable long accountId) {
    return TransactionDtoMapper.TRANSACTION_DTO_MAPPER.fromDomainModelList(_accountManager.getAllTransactions(accountId));
  }
}
