package com.hexa.application.app.dto;

import java.util.Set;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccountDto {

  private Long id;
  private String owner;
  private String name;

}
