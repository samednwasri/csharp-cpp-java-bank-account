package com.hexa.application.app.mapper;


import static org.mapstruct.factory.Mappers.getMapper;

import com.hexa.application.app.dto.AccountBalanceDto;
import com.hexa.domain.model.AccountBalanceModel;
import org.mapstruct.Mapper;

@Mapper
public interface AccountBalanceDtoMapper {
    AccountBalanceDtoMapper ACCOUNTBALANCE_DTO_MAPPER=getMapper(AccountBalanceDtoMapper.class);

    AccountBalanceModel toDomainModel(AccountBalanceDto accountBalance);

    AccountBalanceDto fromDomainModel(AccountBalanceModel accountModel);

}
