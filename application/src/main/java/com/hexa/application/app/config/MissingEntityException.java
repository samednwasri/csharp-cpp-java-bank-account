package com.hexa.application.app.config;

public class MissingEntityException extends RuntimeException{
    public MissingEntityException(String msg){
        super(msg);
    }
}
