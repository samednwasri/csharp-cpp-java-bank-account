package com.hexa.application.app.dto;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TransactionDto {

  private Long id;
  private OffsetDateTime transactionDate;
  private BigDecimal amount;
  private AccountDto account;

}
