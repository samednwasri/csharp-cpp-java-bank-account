package com.hexa.application.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"com.hexa"})
public class KataHexalApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KataHexalApplication.class, args);
    }

}
